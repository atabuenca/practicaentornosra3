package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	
	// Separamos en m�todos los distintos procedimientos que hace nuestro programa.
	
	public static String obtenerPalabra(String rutaFichero) // Obtiene una palabra del fichero dada la ruta y la devuelve al main.
	{
		String palabraSecreta;
		File fich = new File(rutaFichero);
		Scanner inputFichero = null;

		try 
		{
			inputFichero = new Scanner(fich);
			
			for (int i = 0; i < NUM_PALABRAS; i++) 
			{
				palabras[i] = inputFichero.nextLine();	
			}
		} 
		
		catch (Exception e) 
		{
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} 
		
		finally 
		{
			if ((fich != null) && (inputFichero != null)) // Cada condici�n separada entre par�tesis para que quede m�s claro.
			{
				inputFichero.close();
			}
		}
		
		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];
		
		return palabraSecreta;
	}
	
	public static void mostrarPalabra(char[][] caracteresPalabraObtenida) // Muestra la palabra a descubrir.
	{
		// Ponemos saltos de l�nea para que quede m�s ordenado.
		
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabraObtenida[0].length; i++) 
		{
			if (caracteresPalabraObtenida[1][i] != '1') 
			{
				System.out.print(" -");
			} 
			
			else 
			{
				System.out.print(" " + caracteresPalabraObtenida[0][i]);
			}
		}
		
		System.out.println();
	}
	
	public static int analizarFallos(int fallosJugador, String caracteresSeleccionados, boolean estado, char[][] caracteresPalabraObtenida) // Analiza si hay que a�adir un fallo o destapar una letra dado el car�cter seleccionado por el jugador.
	{
		// Ponemos diversos saltos de l�nea para que quede m�s ordenado.
		
		for (int j = 0; j < caracteresSeleccionados.length(); j++) 
		{
			estado = false;
			
			for (int i = 0; i < caracteresPalabraObtenida[0].length; i++) 
			{
				if (caracteresPalabraObtenida[0][i] == caracteresSeleccionados.charAt(j)) 
				{
					caracteresPalabraObtenida[1][i] = '1';
					estado = true;
				}
			}
			
			if (!estado) // Ponemos llaves que engloben lo que hace el if.
			{
				fallosJugador++;
			}
		}
		
		return fallosJugador;
	}
	
	public static void mostrarAhorcado(int fallosJugador) // Pinta el ahorcado dado los fallos del jugador.
	{
		
		// Ponemos un salto de l�nea entre caso y caso para que quede m�s ordenado.
		
		switch (fallosJugador) 
		{
		case 1:
			System.out.println("     ___");
		break;
		
		case 2:
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
		break;
		
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
		break;
		
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
		break;
		}
	}
	
	public static boolean juegoTerminado(int fallosJugador, String palabraObtenidaLista, boolean estado, char[][] caracteresPalabraObtenida) // Analiza los fallos y los car�cteres destapados para devolver un true o false en funci�n de si el juego ha terminado o no.
	{
		// Ponemos saltos de l�nea para que quede m�s ordenado.
		
		if (fallosJugador >= FALLOS) 
		{
			System.out.println("Has perdido: " + palabraObtenidaLista);
			return true;
		}
		
		estado = true;
		
		for (int i = 0; i < caracteresPalabraObtenida[1].length; i++) 
		{
			if (caracteresPalabraObtenida[1][i] != '1') 
			{
				estado = false;
				break;
			}
		}
		
		if (estado)
		{
			System.out.println("Has Acertado ");
			return true;
		}
		
		return false;
	}
	
	
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		String ruta = "src\\palabras.txt";

		String palabraObtenida=obtenerPalabra(ruta);

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraObtenida.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		int fallos=0; // Inicializamos la variable fuera del bucle.
		boolean acertado;
		boolean encontrado=false; // Definimos e inicializamos la variable fuera del bucle.
		
		// Salto de l�nea para que quede m�s limpio.
		
		System.out.println("Acierta la palabra");
		
		// Salto de l�nea para que quede m�s limpio.
		
		do {

			mostrarPalabra(caracteresPalabra);

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			
			fallos=0;
			fallos=analizarFallos(fallos, caracteresElegidos, encontrado, caracteresPalabra);
			
			mostrarAhorcado(fallos);
			
			System.out.println(fallos);
			
			acertado=true;
		} 
		while (!juegoTerminado(fallos, palabraObtenida, acertado, caracteresPalabra));

		input.close();
	}

}