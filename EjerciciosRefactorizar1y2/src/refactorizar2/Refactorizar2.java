package refactorizar2; // Incluimos el paquete donde est� insertado el .java.

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

// import java.io.*; Como no utilizamos este paquete para nada, lo omitimos.
import java.util.Scanner; // Cogemos solo el Scanner porque es el que utilizamos en la clase.

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizar2 { // El nombre de la clase tiene que ser el mismo que el del .java

	// Inicializamos el Scanner en el main y le ponemos un nombre acorde para que sea m�s claro. || static Scanner a;
	
	// Salto de l�nea para que quede m�s limpio.
	
	static Scanner input; // Lo hacemos est�tico fuera del main y le cambiamos el nombre a algo m�s acorde al objeto.
	
	public static void main(String[] args) 
	{
	
		input=new Scanner(System.in);
		
		// int cantidadMaximaAlumnos; Borramos n, inicializamos la variable que cuenta las iteraciones del bucle en el propio for. Nunca barra bajas en las variables. Las palabras juntas con la primera letra de la palabra nueva en may�scula. Quitamo la variable cantidadMaximoAlumnos porque no se utiliza.
		
		// Salto de l�nea para que quede m�s limpio.
		
		int arrays[] = new int[10];
		
		// cantidadMaximaAlumnos = 10;
		
		// Espacio en blanco para separar.
		
		for(int i=0;i<10;i++)
		{
			System.out.println("Introduce nota media de alumno");
			arrays[i] = input.nextInt(); // Adecuamos el nombre de Scanner al nuevo.
		}	
		
		System.out.println("El resultado es: " + recorrerArray(arrays)); // Se adec�a al nombre del m�todo.
		
		input.close(); // Ponemos el nuevo nombre.
	}
	
	// Salto de l�nea para separar.
	
	static double recorrerArray(int vector[]) // Sin barras bajas, las palabras juntas y la primera letra de la nueva palabra en may�scula.
	{
		double numero = 0; // Cambiamos el nombre de la variable a algo m�s acorde.
		
		// Salto de l�nea para que quede limpio
		
		for(int i=0;i<10;i++) // Por convenci�n, el contador de los for empiezan en i para adelante.
		{
			numero=numero+vector[i];
		}
		
		// Salto de l�nea para que quede m�s limpio.
		
		numero=numero/10;
		
		return numero; // En el return solo devolvemos la variable sin operaciones matem�ticas. Para ello igualamos la operaci�n a la variable justo arriba del return.
	}
	
}