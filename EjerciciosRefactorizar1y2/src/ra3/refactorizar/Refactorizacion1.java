package ra3.refactorizar; // Cambiamos el nombre del paquete al que indica el comentario.

/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

// import java.*; Quitamos esta l�nea porque no se utiliza.

import java.util.Scanner; // Importamos la clase Scanner que es la �nica que se usa.

/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
public class Refactorizacion1 { //Siempre la primera letra de la class en may�scula.
	
	final static String BIENVENIDA = "Bienvenido al programa"; // Las variables est�ticas siempre en may�culas, y la renombramos de cad a BIENVENIDA para que sea m�s claro.
	
	// Separamos l�neas de c�digo para que quede m�s claro.
	
	public static void main(String[] args) {
		
		// Espacio en blanco para que quede m�s limpio.
		
		String dni;
		String nombre; // Cambiamos los nombres de las variablels cadena1 y cadena2 por dni y nombre para que tenga m�s sentido. Una declaraci�n en cada l�nea.
		Scanner input = new Scanner(System.in); // Cambiamos el nombre de la variable scanner c por input para que sea m�s claro.
		
		System.out.println(BIENVENIDA);
		System.out.println("Introduce tu dni");
		dni = input.nextLine();
		
		// Separamos l�neas de c�digo para que quede m�s claro.
		
		System.out.println("Introduce tu nombre");
		nombre = input.nextLine();
		
		int numero1=7; // Cambiamos el nombre de las variables a, b y numeroc por numero1, numero2 y numero3 para que quede m�s claro. Las inicializazmos en su declaraci�n, la cual realizamos justo antes de utilizar dichas variables.
		int numero2=16;
		int numero3=25;
		
		if( numero1 > numero2 || (numero3 % 5) != 0 && ((numero3 * 3) - 1) > (numero2 / numero3)) // Un espacio en blanco entre condici�n y condici�n. Separamos las operaciones matem�ticas con par�ntesis.
		{
			System.out.println("Se cumple la condici�n");
		}
		
		numero3 = numero1 + numero2 * numero3 + numero2 / numero1; // Espacio en blanco entre car�cter y car�cter.
		
		String[] diasSemana = {"Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo"}; // Declaramos correctamente el array como String[] array, no como String array[], y le damos sus valores al inicializarlo. Cambiamos el nombre de array a diasSemana para que sea m�s claro.

		/*
 
		array[0] = "Lunes";
		array[1] = "Martes";
		array[2] = "Miercoles";
		array[3] = "Jueves";	Damos su valor a cada hueco del array en su declaraci�n.
		array[4] = "Viernes";
		array[5] = "Sabado";
		array[6] = "Domingo";
		
		*/
		
		recorrerArray(diasSemana); // Los nombres de los m�todos siempre seguidos, la primera letra en min�scula, y la primera letra de la palabra nueva en may�scula.
		
		input.close(); // Cerramos el scanner.
	}
	
	static void recorrerArray(String[] listaNombreDias) // Declaramos correctamente el array de String que utiliza el m�todo. Cambiamos el nombre de la variable de vectordestrings a listaNombreDias para que sea m�s claro.
	{
		for(int i=0;i<7;i++) // En los bucles for, las variables que usamos como contadores siempre se declaran desde la letra i en adelante.
		{
			System.out.println("El dia de la semana en el que te encuentras ["+(i+1)+"-7] es el dia: "+listaNombreDias[i]);
		}
	}
	
}
