package principal;

import java.util.Scanner;

public class Generador {
	
	public static void menu()
	{
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
	
	public static void caracteresAZ(String contrasenia, int tamanio)
	{
		char letra1; // Lo ponemos fuera del bucle para no sobrecargar la memoria.
		
		for (int i = 0; i < tamanio; i++){
			letra1 = (char) ((Math.random() * 26) + 65);
			contrasenia += letra1;
		}
		
		System.out.println(contrasenia);
	}
	
	public static void numeros09(String contrasenia, int tamanio)
	{
		int numero2; // Lo ponemos fuera del bucle para no sobrecargar la memoria.
		
		for (int i = 0; i < tamanio; i++) {
			numero2 = (int) (Math.random() * 10);
			contrasenia += numero2;
		}
		
		System.out.println(contrasenia);
	}
	
	public static void letrasCaracteresEspeciales(String contrasenia, int tamanio)
	{
		int n; // Fuera del bucle para no sobrecargar la memoria.
		char letra3; // Fuera del bucle para no sobrecargar la memoria.
		char caracter3; // Fuera del bucle para no sobrecargar la memoria.
		
		for (int i = 0; i < tamanio; i++) {
			n = (int) (Math.random() * 2);
			
			if (n == 1) {
				letra3 = (char) ((Math.random() * 26) + 65);
				contrasenia += letra3;
			} 
			
			else { // Lo ponemos en otra l�nea para uqe quede m�s limpio.
				caracter3 = (char) ((Math.random() * 15) + 33);
				contrasenia += caracter3;
			}
		}
		
		System.out.println(contrasenia);
	}
	
	public static void letrasNumerosCaracteresEspeciales(String contrasenia, int tamanio)
	{
		int n; // Fuera del bucle para no sobrecargar memoria.
		char caracter3; // Fuera del bucle para no sobrecargar memoria.
		
		for (int i = 0; i < tamanio; i++) {
			n = (int) (Math.random() * 2);
			
			if (n == 1) {
				char letra3;
				letra3 = (char) ((Math.random() * 26) + 65);
				contrasenia += letra3;
			} 
			
			else { // En otra l�nea para que quede m�s limpio.
				caracter3 = (char) ((Math.random() * 15) + 33);
				contrasenia += caracter3;
			}
		}
		
		System.out.println(contrasenia);
	}

	public static void main(String[] args) {
		
		// Salto de l�nea para que quede m�s claro.
		
		Scanner input = new Scanner(System.in); // Cambiamos el nombre de la variable a una m�s adecuada.
		
		// Salto de l�nea para que quede m�s claro.
		
		menu(); // Hacemos un m�todo para que saque el men� por pantalla.
		
		//Salto de l�nea para que quede m�s claro.
		
		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = input.nextInt();
		
		// Salto de l�nea para que quede m�s claro.
		
		System.out.println("Elige tipo de password: ");
		int opcion = input.nextInt();
		
		// Salto de l�nea para que quede m�s claro.
		
		String password = "";
		
		// Salto de l�nea para que quede m�s claro.
		
		switch (opcion) {
		case 1:
			caracteresAZ(password, longitud); // Hacemos un m�todo para simplificar el main.
			break;
			
			// Salto de l�nea para que quede m�s claro.
			
		case 2:
			numeros09(password, longitud); // Hacemos un m�todo para simplifcar el main.
			break;
			
			// Salto de l�nea para que quede m�s claro.
			
		case 3:
			letrasCaracteresEspeciales(password, longitud); // Hacemos un m�todo para simplificar el main.
			break;
			
			// Salto de l�nea para que quede m�s limpio.
			
		case 4:
			letrasNumerosCaracteresEspeciales(password, longitud); // Hacemos un m�todo para simplificar el main.
			break;
		}
		
		// Salto de l�nea para que quede m�s claro.
		
		input.close(); // Cerramos el scanner con el nombre nuevo.
	}

}
